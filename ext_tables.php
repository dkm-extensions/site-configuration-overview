<?php
if (!defined ('TYPO3')) 	die ('Access denied.');
(function() {
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::insertModuleFunction(
		'web_info',
		\DKM\SiteConfigurationOverview\Controller\SiteConfigurationOverviewController::class,
		NULL,
		'LLL:EXT:site_configuration_overview/Resources/Private/Language/locallang_site_configuration_overview.xlf:mod_tx_cms_webinfo_site_configuration_overview'
	);
})();
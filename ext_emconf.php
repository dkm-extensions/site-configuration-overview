<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Web>Info, Site Configuration Overview',
    'description' => 'Backend module which gives an overview of the site configuration from a page in the pagetree.',
    'category' => 'module',
    'author' => 'Stig Nørgaard Færch',
    'author_email' => 'snf@dmk.dk',
    'author_company' => 'DKM',
    'state' => 'stable',
    'version' => '0.2.0',
    'autoload' => [
        'psr-4' => [
            'DKM\\SiteConfigurationOverview\\' => 'Classes'
        ]
    ]
];

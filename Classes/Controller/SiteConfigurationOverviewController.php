<?php

namespace DKM\SiteConfigurationOverview\Controller;

use TYPO3\CMS\Core\Configuration\Loader\YamlFileLoader;
use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\TypoScript\ExtendedTemplateService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Info\Controller\InfoModuleController;

/**
 * Class for displaying a tree view of the site configuration
 */
class SiteConfigurationOverviewController
{

    /**
     * @var int Value of the GET/POST var 'id'
     */
    protected $id;

    /**
     * @var InfoModuleController Contains a reference to the parent calling object
     */
    protected $pObj;

    /**
     * Constructor
     */
    public function __construct()
    {
        if (isset($GLOBALS['BE_USER']->uc['titleLen']) && $GLOBALS['BE_USER']->uc['titleLen'] > 0) {
            $this->fixedL = $GLOBALS['BE_USER']->uc['titleLen'];
        }
    }

    /**
     * Init, called from parent object
     *
     * @param InfoModuleController $pObj A reference to the parent (calling) object
     */
    public function init($pObj)
    {
        $this->pObj = $pObj;
        $this->id = (int)GeneralUtility::_GP('id');
        // Setting MOD_MENU items as we need them for logging:
        $this->pObj->MOD_MENU = array_merge($this->pObj->MOD_MENU, $this->modMenu());
    }

    /**
     * Returns the menu array
     *
     * @return array
     */
    public function modMenu()
    {
        return [];
    }

    /**
     * MAIN function for page information of localization
     *
     * @return string Output HTML for the module.
     */
    public function main()
    {
        /** @var Site $site */
        $site = $GLOBALS['TYPO3_REQUEST']->getAttribute('site');

        /** @var ExtendedTemplateService $tmpl */
        $tmpl = GeneralUtility::makeInstance(ExtendedTemplateService::class);
        $tmpl->ext_expandAllNotes = 1;
        $tmpl->ext_noPMicons = 1;

        $arrayBrowser = GeneralUtility::makeInstance(\TYPO3\CMS\Backend\View\ArrayBrowser::class);
        $arrayBrowser->dontLinkVar = true;
        $arrayBrowser->searchKeysToo = true;
        $arrayBrowser->expAll = true;

        switch (get_class($site)) {
            case 'TYPO3\CMS\Core\Site\Entity\NullSite':
            case 'TYPO3\CMS\Core\Site\Entity\PseudoSite':
                return '<p>No site configured here!</p>';
            default:
                $host = $site->getBase()->getHost();
                $identifier = $site->getIdentifier();
                $pageUid = $site->getRootPageId();
                $renderArray = GeneralUtility::makeInstance(\DKM\SiteConfigurationOverview\Configuration\SiteConfiguration::class)->load($site->getIdentifier());
                return "<p><strong>Site identifier:</strong> {$identifier}<br/><strong>Root page uid:</strong> {$pageUid}</br><strong>Host:</strong> {$host}</p>" .
                    $arrayBrowser->tree($renderArray, '');
        }
    }
}
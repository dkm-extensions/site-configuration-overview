<?php

namespace DKM\SiteConfigurationOverview\Configuration;

use TYPO3\CMS\Core\Configuration\Loader\YamlFileLoader;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SiteConfiguration extends \TYPO3\CMS\Core\Configuration\SiteConfiguration
{
    public function load(string $siteIdentifier): array
    {
        $fileName = $this->configPath . '/' . $siteIdentifier . '/' . $this->configFileName;
        $loader = GeneralUtility::makeInstance(YamlFileLoader::class);
        $flags = YamlFileLoader::PROCESS_IMPORTS + YamlFileLoader::PROCESS_PLACEHOLDERS;
        return $loader->load(GeneralUtility::fixWindowsFilePath($fileName), $flags);
    }
}